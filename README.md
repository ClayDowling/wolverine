# Test Driven Development for Embedded Software

In this workshop we're going to test drive code to flash an LED (or two) using at ATMega328p microprocessor.  For simplicity we'll use an Arduino Nano clone to save wiring in a power supply and programmer.

## Circuit

The official Pinout for the Arduino Nano (and clones) is at https://content.arduino.cc/assets/Pinout-NANO_latest.png

We'll be using a pre-constructed circuit that conforms to this diagram:

![Schematic](resources/wolverineflasher_schem.png)

![Breadboard](resources/wolverineflasher_bb.png)

For programming purposes, the blue led is connected to PD2, and the yellow led is connected to PD3.

## Software

We'll be using gcc's AVR toolchain to compile and link our softare.  CMake will be used to manage both the target build and the test build.

Testing is done using the [Unity](http://www.throwtheswitch.org/unity) testing suite.

## CI/CD Pipeline

On GitLab, all PRs merging to master will have the test suite run.

On a tagged release, artifacts will be built and stored.


## Technical Details

As written, this lab uses an Atmel ATMega328 processor, which uses the AVR instruction set.  To control pins on a AVR device, you need to do the following:

### Setup

1. Set up the Data Direction Register to treat the pin as input or output.  Data direction registers have names like DDR[A|B|C|D].  You set a pin as output by setting the matching bit in the register high.

2. Each pin has a variable that represents the correct bit pattern to set it in the DDR register.  So Data Direction for pin D3 is DDD3.

3. To set a single bit high in a register without affecting other bits, the code looks like:

    DDRD |= _BV(DDD3);

4. To set a single bit low without affecting other bits, the code looks like:

    DDRD &= ~_BV(DDD3);

### Controlling

1. Controlling a pin happents on the port register associated with the pin.  So For D2, you would set bit 2 on PORTD high or low.

2. The bit pattern for each pin has a macro named PORT[port letter][pin], so ping 2 on port D is PORTD2

3. Value setting for port registers uses identical syntax to the Data Direction Registers.

### Standard Library

For embedded, the standard C library isn't appropriate.  There is a free library with commonly needed functions available for AVR compatible microcontrollers, called [avr libc](https://www.nongnu.org/avr-libc/user-manual/).

## Exercise Goals

1. Using Test first development, create firmware that alternates between the blue and yellow LEDs being lit.  Each LED should stay lit for half a second.

2. Attach a momentary switch to D4.  The device should take no action until the button is pressed and released.  It should perform the blue and yellow cycle from Goal 1 for one minute, and then go dark again.